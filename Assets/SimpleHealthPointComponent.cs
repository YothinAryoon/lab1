using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Aryoon.GameDev3.Chapter1
{
    public class SimpleHealthPointComponent : MonoBehaviour
    {
        [SerializeField]
        public const float MAX_HP = 100;
        [SerializeField]
        private float m_HealthPoint;

        public float HealthPoint
        {
            get
            {
                return m_HealthPoint;
            }
            set 
            {
                if (value > 0)
                {
                    if (value <= MAX_HP)
                    {
                        m_HealthPoint = value;
                    }
                    else
                    {
                        m_HealthPoint = MAX_HP;
                    }
                }
            }
        }
        
        void Start()
        {
        
        }

        // Update is called once per frame
        void Update()
        {
        
        }
    }
}

